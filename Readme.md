### elenco-albero

    convert list to tree-like nested list
    
    e.g. [a, b, c, d, e] to [a [b], c[d,e]]   
      (actual input / output types differ)
      
    Possible application: tree-like menu
                          in web apps.
