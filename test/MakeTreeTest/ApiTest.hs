module MakeTreeTest.ApiTest where

import Test.Hspec
import Data.Elenco.Api
import Data.Elenco.Type.In
import Data.Elenco.Type.Out
import Debug.Trace


main::IO()
main = hspec $ do
       describe "MakeTreeTest.ApiTest" $ do
          it "make tree" $ do
            traceIO $ show out1
            out1 `shouldBe` out2
        where in1 = [In 1 Nothing,
                    In 11 (Just 1),
                    In 3 Nothing,
                    In 112 (Just 11)]
              out1 = nest in1
              out2 = [Out 1 Nothing [
                      Out 11 (Just 1)
                        [Out 112 (Just 11) []]],
                      Out 3 Nothing []
                        ]

