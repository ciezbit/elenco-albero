module Data.Elenco.Api
    (nest) where

import qualified Data.Elenco.Albero as A
import Data.Elenco.Type.In as I
import Data.Elenco.Type.Out as O


nest::Eq a =>
  [In a] -> [Out a]
nest in0 = A.nest $ out1 <$> in0
  where out1 in1 = Out {
                      O.this = I.this in1,
                      O.parent = I.parent in1,
                      children = [] }
