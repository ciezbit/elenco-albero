module Data.Elenco.Type.In where

data In a = In {
    this::a,
    parent::Maybe a
} deriving (Eq, Show)
