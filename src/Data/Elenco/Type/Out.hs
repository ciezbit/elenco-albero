module Data.Elenco.Type.Out where

data Out a = Out {
    this::a,
    parent::Maybe a,
    children::[Out a]
} deriving (Eq, Show)
