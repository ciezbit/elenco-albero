module Data.Elenco.Albero
  (nest) where

import Data.Elenco.Type.Out as T
import Data.List as L


nest::Eq a =>
  [Out a] -> [Out a]
nest acc0 =
      if length acc0 == length acc1
        then acc1
        else nest acc1  --  repeat
    where leaves1 = L.filter (noChildren acc0) acc0
          acc1 = L.foldl attachToParent acc0 leaves1


attachToParent::Eq a =>
  [Out a] -> Out a -> [Out a]
attachToParent acc0 this0 =
    let mparent1 = findParent acc0 this0
        parentExist2 parent2 = attachToParent_
                                  (parent2, this0) acc0
    in maybe acc0 parentExist2 mparent1

attachToParent_::Eq a =>
  (Out a, Out a) -> [Out a] -> [Out a]
attachToParent_ (parent0, child0) list0 =
  let (parent1, exParent1) = L.partition (== parent0) list0
      (child1, exChild1) = L.partition (== child0) exParent1
      bothFound2 (parent2, child2) = parent3 : exChild1
              where parent3 = parent2 { children = child2
                                        : (children parent2)
                                      }
      mparentChild3 = head' parent1 >>=
                    \parent2 -> head' child1 >>=
                    \child2 -> Just (parent2, child2)
  in maybe list0 bothFound2 mparentChild3


findParent::Eq a =>
  [Out a] -> Out a -> Maybe (Out a)
findParent acc0 this0 = L.find (b_isParent_a this0) acc0


noChildren::Eq a =>
  [Out a] -> Out a -> Bool
noChildren acc0 n0 = not any2
    where pred1 = b_isChild_a n0
          any2 = L.any pred1 acc0

b_isChild_a::Eq a =>
  Out a -> Out a -> Bool
b_isChild_a a0 b0 = maybe False (== (T.this a0)) $ parent b0

b_isParent_a::Eq a =>
  Out a -> Out a -> Bool
b_isParent_a = flip b_isChild_a


head'::[a] -> Maybe a
head'[] = Nothing
head' l0 = Just $ head l0
